package com.cross.site.forge.servlet;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class ServletClass {
	
	Logger log = Logger.getLogger(this.getClass().getName());

	@RequestMapping("/sample")
	public RedirectView processApplication(HttpServletRequest request) {
		String accountNumber = request.getParameter("accountNumber");
		String routingNumber = request.getParameter("routingNumber");
		 log.info("User Account Number:"+accountNumber);
		 log.info("User Routing PIN Number:"+routingNumber);
		 return new RedirectView("./Fired.html");
	}
}
