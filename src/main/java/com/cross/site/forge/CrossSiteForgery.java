package com.cross.site.forge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrossSiteForgery {
	public static void main(String[] args) {
		SpringApplication.run(CrossSiteForgery.class, args);
	}
}
